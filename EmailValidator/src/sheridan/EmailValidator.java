package sheridan;

public class EmailValidator {
	
	public static boolean isValidEmail(String email) {
		
		if(email != null) {
			return email.matches("^[^@\\s0-9](\\w*[a-zA-Z]\\w*[a-zA-Z]\\w*)"
					+ "+@([a-z0-9]\\w*[a-z0-9]\\w*[a-z0-9])"
					+ "+\\.([A-Za-z]\\w*[A-Za-z])+$");
		}
		return false;
	}

}
