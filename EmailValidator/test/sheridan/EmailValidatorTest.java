package sheridan;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class EmailValidatorTest {

	
	// Testing for email format validation
	@Test
	public void testIsValidEmailFormatSymbolRegular() {
		assertTrue("Email symbol is invalid", EmailValidator.isValidEmail("test@email.email"));
	}
	
	@Test
	public void testIsValidEmailFormatSymbolException() {
		assertFalse("Email symbol is invalid", EmailValidator.isValidEmail("test@email"));
	}
	
	@Test
	public void testIsValidEmailFormatSymbolBoundaryOut() {
		assertFalse("Email symbol is invalid", EmailValidator.isValidEmail("test@email.@email"));
	}
	
	@Test
	public void testIsValidEmailFormatSymbolBoundaryIn() {
		assertTrue("Email symbol is invalid", EmailValidator.isValidEmail("test@email.email"));
	}
	
	// Testing for email account name validation
	@Test
	public void testIsValidEmailAccountNameRegular() {
		assertTrue("Email account name is invalid", EmailValidator.isValidEmail("Tes11t@email.email"));
	}
	
	@Test
	public void testIsValidEmailAccountNameException() {
		assertFalse("Email account name is invalid", EmailValidator.isValidEmail("1App00@email.email"));
	}
	
	@Test
	public void testIsValidEmailAccountNameBoundaryOut() {
		assertFalse("Email account name is invalid", EmailValidator.isValidEmail("pp.@email.email"));
	}
	
	@Test
	public void testIsValidEmailAccountNameBoundaryIn() {
		assertTrue("Email account name is invalid", EmailValidator.isValidEmail("ppp@email.email"));
	}
	
	// Testing for email domain name validation
	@Test
	public void testIsValidEmailDomainNameRegular() {
		assertTrue("Email domain name is invalid", EmailValidator.isValidEmail("Tes11t@email1.email"));
	}
	
	@Test
	public void testIsValidEmailDomainNameException() {
		assertFalse("Email domain name is invalid", EmailValidator.isValidEmail("Tes11t@EMAIL1.email"));
	}
	
	@Test
	public void testIsValidEmailDomainNameBoundaryOut() {
		assertFalse("Email domain name is invalid", EmailValidator.isValidEmail("Tes11t@eMAIL1.email"));
	}
	
	@Test
	public void testIsValidEmailDomainNameBoundaryIn() {
		assertTrue("Email domain name is invalid", EmailValidator.isValidEmail("Tes11t@em1.email"));
	}
	
	// Testing for email extension name validation
	@Test
	public void testIsValidEmailExtensionNameRegular() {
		assertTrue("Email extension name is invalid", EmailValidator.isValidEmail("Tes11t@email1.email"));
	}
	
	@Test
	public void testIsValidEmailExtensionNameException() {
		assertFalse("Email extension name is invalid", EmailValidator.isValidEmail("Tes11t@email1.111"));
	}
	
	@Test
	public void testIsValidEmailExtensionNameBoundaryOut() {
		assertFalse("Email extension name is invalid", EmailValidator.isValidEmail("Tes11t@email1.e"));
	}
	
	@Test
	public void testIsValidEmailExtensionNameBoundaryIn() {
		assertTrue("Email extension name is invalid", EmailValidator.isValidEmail("Tes11t@email1.em"));
	}
	
}
